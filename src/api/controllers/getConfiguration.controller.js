// export
exports.getConfiguration = (req, res, next) => {
    return res.json(
        {
            "status": 1,
            "idleTime": 900,
            "deviceInfo": {
                "deviceID": "tuy.hgonbt.016",
            },
            "forceUpdate": {
              "enable": true,
              "updateType": "major",
              "versionIOS": "xxx",
              "iOSUpdateUrl": "https://itunes.apple.com/hk/app/xxxxx",
              "versionAndroid": "xxx",
              "androidUpdateUrl": "https://play.google.com/store/apps/details?id=com.hanglung.hanglungmalls"
            },
            "maintenance": {
              "enable": true,
              "redirect": {
                "en": "https://www.hanglung.com/en-us/",
                "tc": "https://www.hanglung.com/zh-hk/",
                "sc": "https://www.hanglung.com/zh-cn/"
              },
              "title": {
                "en": "Oops!",
                "tc": "系統提示",
                "sc": "系统提示"
              },
              "msg": {
                "en": "Sorry!XXXXX",
                "tc": "XXXXXX",
                "sc": "XXXXXX"
              }
            }
          }
    );
  }